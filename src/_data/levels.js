const recursive = require("recursive-readdir");


module.exports = async function() {

    let files = await recursive("src/_includes/levels/");
    return files.map(val => {
        let out = val.replace(/src\/\_includes\/levels\//g, '')
        out = out.replace(/\.js$/, "")
        return out
    })
}
