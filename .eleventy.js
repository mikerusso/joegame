const CleanCSS = require("clean-css");
const UglifyJS = require("uglify-js");
const path = require("path")

module.exports = function(eleventyConfig) {

    eleventyConfig.addPassthroughCopy("assets")
    eleventyConfig.addPassthroughCopy({ "pwa": "/" })
    // eleventyConfig.addPassthroughCopy("pwa/*")

    eleventyConfig.addFilter("cssmin", function(code) {
        return new CleanCSS({}).minify(code).styles;
    });

    eleventyConfig.addFilter("jsmin", function(code) {
        return new UglifyJS.minify(code).code;
    });

    // eleventyConfig.addFilter("pathToURL", function(jspath) {
    //     let out = jspath.replace(/src\/\_includes\/levels\//g, '')
    //     out = out.replace(/\.js$/, "")
    //     console.log(out)
    // });

    return {
        dir: {
            input: "src",
            output: "public"
        }
    }
};
